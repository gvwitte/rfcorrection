clear, close all

pi = 3.1415926535897932384626433;
kB = 1.380649e-23;
h = 6.62607015e-34;
g_e = 28024.952e6; %gyromagnetic ratio electrons

set(0,'DefaultAxesFontSize', 20)
set(0,'DefaultTextFontSize', 16)
set(0,'DefaultFigureUnits','centimeter','DefaultFigurePosition',[0 0 24 18])
%% Input parameters
T = 3.4;                           % Experimental temperature in [K]
B = 7;                             % Experimental magnetic field in [T]
P_e = 100*tanh(g_e*h*B/(2*kB*T));  % Thermal electron polarization

Flip_angle = 2.44;       % Flip angle in the experiment in [degree]
RF_time = 1;             % RF time = time between RF pulses in time units of the build-ups and decays

nExp = 328;              % Total number of data points
MWon = 1;                % Microwave switched on before this data point (begin of build-up)
MWoff = 138;              % Micorwave switched off after this data point (end of build-up and begin of decay)

%%  Load preprocessed data set and organise data
% This needs to be adjusted for the given experimental set-up
% Currently this script expects a file with the time in the first column
% and the measured DNP signal (in arbitrary units) in the second column

expData = load('dnpArea_uncorr.dat'); % Specify path to data

timeBup = expData(MWon:MWoff,1)';
bupData = expData(MWon:MWoff,2)';  %time domain
if nExp > (MWoff+7)
    timeDecay = (expData((MWoff+1):nExp,1) - expData((MWoff+1),1))';
    decayData = (expData((MWoff+1):nExp,2))';  
else
    decayData = [];
end

cosPhi = cos(Flip_angle*pi/180);
iCosPhi = 1/cosPhi;

%% Applying the self-consistent algorithm to the build-up data
[FitBup, FitParametersBup] = FitBuildupRF(timeBup,bupData);  % Fit uncorrected build-up


[bupData_corr,FitParametersBup_corr,CCmodelBup] = CorrectionBup(timeBup,bupData,cosPhi,RF_time,P_e); % Apply iterative correction
fitCurveBup = FitParametersBup_corr(3)*(1-exp(-timeBup/FitParametersBup_corr(5)))+FitParametersBup_corr(7); 

close(figure(1))
fg1 = figure(1);
    plot(timeBup, bupData,'.k','MarkerSize',20, 'LineWidth', 1)
    hold on
    plot(timeBup, bupData_corr,'.r', 'MarkerSize',20, 'LineWidth', 1)
    plot(timeBup, fitCurveBup, '-r', 'LineWidth', 2)
    hold off
    grid on
    legend({'raw data', 'corr. data'}, 'Location', 'southeast');
    xlabel('Time [s]')
    ylabel('Signal [a.u.]')
% exportgraphics(fg1,'bupCorr.pdf','BackgroundColor','none')

%% Applying the self-consistent algorithm to the decay data
if 0 == isempty(decayData) 
    [FitDecay, FitParametersDecay] = FitDecayRF(timeDecay,decayData); % Fit uncorrected decay
    
    [decayData_corr,FitParametersDecay_corr,CCmodelDecay] = CorrectionDecay(timeDecay,decayData,cosPhi,RF_time); % Apply iterative correction
     fitCurveDecay = FitParametersDecay_corr(3)*exp(-timeDecay/FitParametersDecay_corr(5))+FitParametersDecay_corr(7);
  
    for i=1:length(timeDecay)
        decayData_cosCorr(i) = decayData(i)*iCosPhi^i; % Apply 1/cos^n correction
    end
    [FitCosCorr, FitParametersDecay_CosCorr] = FitDecayRF(timeDecay,decayData_cosCorr); % Fit 1/cos^n corrected data

    close(figure(2))
    fg2 = figure(2);
        plot(timeDecay, decayData,'.k','MarkerSize',20, 'LineWidth', 1)
        hold on
        plot(timeDecay, decayData_corr,'.r','MarkerSize',20, 'LineWidth', 1)
        plot(timeDecay, decayData_cosCorr,'.g','MarkerSize',20, 'LineWidth', 1)
        plot(timeDecay, fitCurveDecay, '-r', 'LineWidth', 2)
        hold off
        grid on
        legend({'raw data', 'corr. data', 'cos corr.'}, 'Location', 'northeast');
        xlabel('Time [s]')
        ylabel('Signal [a.u.]')
        ylim([0 1.1*max(decayData)])
%     exportgraphics(fg2,'decayCorr.pdf','BackgroundColor','none')
end

%% Functions
function [dataCorr,FitParameters,CC_model] = CorrectionBup(timeAxis,bupData,CosPhi,RFtime,a)   % Correction algorithm for the build-up

    dataCorr = bupData;

    % Initialize corrections 
    [FitResults, FitParameters] = FitBuildupRF(timeAxis,bupData);
    
    % Perform CC-correction
    CC_model(1) = (1/FitParameters(5) + log(CosPhi)/RFtime)^(-1);   % Build-up time
    k_W = FitParameters(3)/a*FitParameters(5)^-1;
    CC_model(2) = a*k_W*CC_model(1);                                % steady-state polarization


    for i=2:length(timeAxis)
        dataCorr(i) = dataCorr(i-1) + (bupData(i)-CosPhi*bupData(i-1)) - (dataCorr(i-1)-CosPhi*bupData(i-1))*CC_model(1)^(-1)*RFtime;
    end
    
    [FitResults, FitParameters] = FitBuildupRF(timeAxis,dataCorr);
    
end

function [dataCorr,FitParameters,CC_model] = CorrectionDecay(timeAxis,decayData,CosPhi,RFtime)   % Correction algorithm for the decay

    dataCorr = decayData;

    % Initialize corrections
    [FitResults, FitParameters] = FitDecayRF(timeAxis,decayData);
    CC_model = (1/FitParameters(5) + log(CosPhi)/RFtime)^(-1);   % Decay time

    % Start actual algorithm
    for i=2:length(timeAxis)
        dataCorr(i) = dataCorr(i-1) + (decayData(i)-CosPhi*decayData(i-1)) - (dataCorr(i-1)-CosPhi*decayData(i-1))*CC_model^(-1)*RFtime;
    end
    
    [FitResults, FitParameters] = FitDecayRF(timeAxis,dataCorr);
    
end

function [fitDNP, fitDNPsummary] = FitBuildupRF(timeAxis,dnpSignal)  % Monoexponential Build-up

    fitDNP = zeros(length(timeAxis));                  % Data of the fitted build-up
    fitDNPsummary = zeros(8,1);                        % Summary of the goodness-of-fit, fit parameters confidence intervals

    funDNPmono = 'p1*(1-exp(-x/p2))+p3';
    options = fitoptions(funDNPmono, 'Display', 'off');
    options.StartPoint = [2*max(dnpSignal(:)),max(timeAxis)*2, 0];
    options.Lower = [0.0,0.0,0.0];
    options.Upper = [Inf,Inf,Inf];

    [fitobjectDNPmono, gofDNPmono] = fit(timeAxis.',dnpSignal.', funDNPmono, options);   % actual fit / time vs a
    fitDNP = fitobjectDNPmono.p1*(1-exp(-timeAxis/fitobjectDNPmono.p2))+fitobjectDNPmono.p3; 
    
    % Store ordered fit parameters
    ciDNPmono = confint(fitobjectDNPmono);
    
    fitDNPsummary(1) = gofDNPmono.rsquare;
    fitDNPsummary(2) = gofDNPmono.adjrsquare;
    fitDNPsummary(3) = fitobjectDNPmono.p1;
    fitDNPsummary(4) = abs((ciDNPmono(2,1)-ciDNPmono(1,1)))/2;
    fitDNPsummary(5) = fitobjectDNPmono.p2;
    fitDNPsummary(6) = abs((ciDNPmono(2,2)-ciDNPmono(1,2)))/2;
    fitDNPsummary(7) = fitobjectDNPmono.p3;
    fitDNPsummary(8) = abs((ciDNPmono(2,3)-ciDNPmono(1,3)))/2;
end


function [fitDecay, fitDecaySummary] = FitDecayRF(timeAxis,dnpSignal) % Monoexponential decay

    fitDecay = zeros(length(timeAxis));                  % Data of the fitted build-up
    fitDecaySummary = zeros(8,1);                        % Summary of the goodness-of-fit, fit parameters confidence intervals

    funDecayMono = 'p1*exp(-x/p2)+p3';
    options = fitoptions(funDecayMono, 'Display', 'off');
    options.StartPoint = [2*max(dnpSignal(:)),max(timeAxis)/4,0];
    options.Lower = [0.0,0.0,0.0];
    options.Upper = [Inf,Inf,Inf];

    [fitobjectDecayMono, gofDecayMono] = fit(timeAxis.',dnpSignal.', funDecayMono, options);   % actual fit / time vs a
    fitDecay = fitobjectDecayMono.p1*(1-exp(-timeAxis/fitobjectDecayMono.p2))+fitobjectDecayMono.p3; 
    
    % Store ordered fit parameters
    ciDecayMono = confint(fitobjectDecayMono);
    
    fitDecaySummary(1) = gofDecayMono.rsquare;
    fitDecaySummary(2) = gofDecayMono.adjrsquare;
    fitDecaySummary(3) = fitobjectDecayMono.p1;
    fitDecaySummary(4) = abs((ciDecayMono(2,1)-ciDecayMono(1,1)))/2;
    fitDecaySummary(5) = fitobjectDecayMono.p2;
    fitDecaySummary(6) = abs((ciDecayMono(2,2)-ciDecayMono(1,2)))/2;
    fitDecaySummary(7) = fitobjectDecayMono.p3;
    fitDecaySummary(8) = abs((ciDecayMono(2,3)-ciDecayMono(1,3)))/2;
end