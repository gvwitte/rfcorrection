# RFcorrection

Matlab postprocessing script to correct for the effects of readout RF pulses in hyperpolarized NMR. The script is associated with the paper titled "Modelling and correcting the impact of readout RF pulses in hyperpolarized NMR" by Gevin von Witte, Matthias Ernst and Sebastian Kozerke in 2023

## Installation
Standalone Matlab script with example data set included in the repository (settings in the script upon download are adjusted to this data set). The script only needs a file with two columns besides several experimental parameters (more details in the script itself): first the time of the acquisitions and, second, the measured hyperpolarized signal

## Support
If you encounter problems, let me know: vonwitte@biomed.ee.ethz.ch or whatever email adress of me you can find. Alternatively, contact the corresponding author of the paper (Sebastian Kozerke: kozerke@biomed.ee.ethz.ch)

## License
Same license as the paper it is associated with
